import {CLICK_NUMBER, CLICK_OPERATION} from "./actionTypes";

let op = (op, v1, v2) => {
    let result;
    switch(op) {
        case '+':
            result = v1 + v2;
            break;
        case '-':
            result = v1 - v2;
            break;
        case '*':
            result = v1 * v2;
            break;
        case '/':
            result = v1 / v2;
            break;
    }
    return {
        result,
        error: Number.isNaN(result) || !Number.isFinite(result)
    }
};

function ClickNumber(state, action) {
    console.log('click_number', action.number);
    if(state.displayNumber === 0 || state.displayNumber === 'E') {
        return Object.assign({}, state, {
            isEnterNewNumber: false,
            arg1: action.number,
            displayNumber: action.number
        });
    } else if(state.isEnterNewNumber) {
        return Object.assign({}, state, {
            isEnterNewNumber: false,
            arg1: state.displayNumber,
            arg2: action.number,
            displayNumber: action.number
        });
    } else {
        let currentNumber = parseInt(state.displayNumber.toString() + action.number.toString());
        if(state.arg2) {
            return Object.assign({}, state, {
                arg2: currentNumber,
                displayNumber: currentNumber
            });
        } else {
            return Object.assign({}, state, {
                arg1: currentNumber,
                displayNumber: currentNumber
            });
        }
    }
}

function ClickOperation(state, action) {
    console.log('click_operation', action.operation);
    switch(action.operation) {
        case '+':
        case '-':
        case '*':
        case '/':
            if(state.arg1 !== undefined && state.arg2 === undefined) {
                return Object.assign({}, state, {
                    operation: action.operation,
                    isEnterNewNumber: true,
                });
            } else if(state.arg1 !== undefined && state.arg2 !== undefined) {
                let {result, error} = op(state.operation, state.arg1, state.arg2);
                if(error) {
                    return {displayNumber: 'E'}
                } else {
                    return Object.assign({}, state, {
                        isEnterNewNumber: true,
                        operation: action.operation,
                        arg1: result,
                        arg2: undefined,
                        displayNumber: result
                    });
                }
            }
            break;
        case '=':
            if(state.arg1 !== undefined && state.arg2 !== undefined) {
                let {result, error} = op(state.operation, state.arg1, state.arg2);
                if(error) {
                    return {displayNumber: 'E'};
                } else {
                    return Object.assign({}, state, {
                        arg1: result,
                        isEnterNewNumber: true,
                        displayNumber: result
                    });
                }
            }
            break;
        case 'C':
            return {displayNumber: 0};
        default:
            console.log('Invalid operation');
            return state;
    }
}

export default function(state = {displayNumber: 0}, action = {type: "NO_ACTION"}) {
    switch(action.type) {
        case CLICK_NUMBER:
            return ClickNumber(state, action);
            break;
        case CLICK_OPERATION:
            return ClickOperation(state, action);
            break;
        default:
            console.log('default_action', action);
            return state;
    }
    return state;
}