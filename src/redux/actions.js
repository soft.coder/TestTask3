import {CLICK_NUMBER, CLICK_OPERATION} from './actionTypes';

export const clickNumber = number => ({type: CLICK_NUMBER, number: number});
export const clickOperation = operation => ({type: CLICK_OPERATION, operation: operation});
