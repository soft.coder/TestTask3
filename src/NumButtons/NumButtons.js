import React from "react";
import './NumButtons.css';
import NumberButton from '../Button/NumButton/NumButton';
import OperationButton from '../Button/OperationButton/OperationButton';

const NumButtons = () => {
    let nums = [1, 2, 3];
    return (
        <div className="num-buttons">
            {
                nums.map(v =>
                    (<div className="num-buttons__row" key={v}>
                        {
                            nums.map(i =>
                                (<NumberButton text={(v - 1) * 3 + i} key={i}/>)
                            )
                        }
                    </div>))
            }
            <div className="num-buttons__bottom">
                <NumberButton text={0}/>
                <OperationButton text="C"/>
                <OperationButton text="="/>
            </div>
        </div>
    )
};

export default NumButtons;
