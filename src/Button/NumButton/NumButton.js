import { connect } from 'react-redux';
import Button from '../Button';
import {clickNumber} from '../../redux/actions';

const mapDispatchToProps = (dispatch, {text}) => {
    return {
        text: text,
        onClick: number => {
            dispatch(clickNumber(parseInt(number)));
        }
    }
};
const NumberButton = connect(
    undefined,
    mapDispatchToProps
)(Button);

export default NumberButton