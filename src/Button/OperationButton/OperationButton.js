import { connect } from 'react-redux';
import Button from '../Button';
import {clickOperation} from '../../redux/actions';


const mapDispatchToProps = (dispatch, {text}) => {
    return {
        text: text,
        onClick: operation => {
            dispatch(clickOperation(operation));
        }
    }
};

const OperationButton = connect(
    undefined,
    mapDispatchToProps
)(Button);

export default OperationButton