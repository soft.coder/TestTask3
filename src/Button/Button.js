import React from "react";
import './Button.css';

const Button = ({text, onClick}) => {
    return (
        <button className="button" onClick={() => onClick(text)}>
            <div className="button__text">
                {text}
            </div>
        </button>
    );
};

export default Button;
