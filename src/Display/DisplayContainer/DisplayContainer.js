import {connect} from "react-redux";
import Display from '../Display';

const mapStateToProps = state => {
    console.log('state', state);
    return {
        number: state.displayNumber,
    }
};

let DisplayContainer = connect(mapStateToProps)(Display);

export default DisplayContainer;