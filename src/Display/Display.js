import React from "react";
import './Display.css';

const Display = ({number}) => {
    return (
        <div className="display">
            <div className="display__number">
                <p>{number.toString().length > 15 ? number.toExponential(10) : number}</p>
            </div>
        </div>
    );
};

export default Display;
