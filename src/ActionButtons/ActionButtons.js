import React from "react";
import './ActionButtons.css';
import OperationButton from '../Button/OperationButton/OperationButton';

const ActionButtons = () => {
    return (
        <div className="action-buttons">
            <OperationButton text="+" />
            <OperationButton text="-" />
            <OperationButton text="*" />
            <OperationButton text="/" />
        </div>
    );
};

export default ActionButtons;
