import React from "react";
import './Calc.css';
import NumButtons from "../NumButtons/NumButtons";
import ActionButtons from "../ActionButtons/ActionButtons";
import DisplayContainer from "../Display/DisplayContainer/DisplayContainer";

const Calc = () => {
    return (
        <div className="calc">
            <div className="calc__display">
                <DisplayContainer/>
            </div>
            <div className="calc__buttons">
                <NumButtons />
                <ActionButtons />
            </div>
        </div>
    );
};

export default Calc;
