import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Calc from './Calc/Calc';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React Calculator</h1>
        </header>
        <p className="App-intro">
            &nbsp;
        </p>
          <div className="calc-wrapper">
        <Calc></Calc>
          </div>
      </div>
    );
  }
}

export default App;
